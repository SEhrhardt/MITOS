<div id="Begin" style="display:none">
	Uploading... <br>
</div>

<div id="Finish" style="display:none">
	Upload finished!<br>
	You will be redirected in 5 seconds.<br>
</div>

<div id="alpha" style="display:none">
	Error! <br>
	The provided sequence seems to be no nucleotide sequence.<br><br>
	<a href="#" onClick="history.go(-1);return true;">Back to MITOS start page.</a>
</div>

<div id="multi" style="display:none">
	Error!<br>
	Per default MITOS does not annotate multiple sequences in a single FASTA file.<br>
	You may change this in the advanced options. Please make sure that you read the
	<a href="http://{mitosurl}/help.py#advanced">documentation</a> first.</a><br><br>
	Alternatively provide each sequence in a separate file.<br><br>
	<a href="#" onClick="history.go(-1);return true;">Back to MITOS start page.</a>
</div>

<div id="nofasta" style="display:none">
	Error!<br> 
	Please provide a file in FASTA format.<br><br>
	<a href="#" onClick="history.go(-1);return true;">Back to MITOS start page.</a>
</div>

<div id="empty" style="display:none">
	Error!<br> 
	The provided sequence is empty.<br><br>
	<a href="#" onClick="history.go(-1);return true;">Back to MITOS start page.</a>
</div>

<div id="maxsize" style="display:none">
<br>
	Sequence to large!<br><br>
	The sequence you have uploaded would probably block the MITOS webserver for a long time. <br>
	We would like to offer you to run the MITOS analysis on dedicated hardware that is independent of the webserver. <br>   
	Please contact us at <a href="mailto:{email}">{email}</a>.<br><br>
	<a href="#" onClick="history.go(-1);return true;">Back to MITOS start page.</a>
</div>

<div id="minsize" style="display:none">
<br>
	Sequence to short!<br><br>
	<br>
	We would like to offer you to run the MITOS analysis on hardware that is independent of the webserver. <br>
	Please contact us at <a href="mailto:{email}">{email}</a>.<br><br>
	<a href="#" onClick="history.go(-1);return true;">Back to MITOS start page.</a>
</div>

<div id="largefasta" style="display:none">
<br>
	To many sequences are contained in the uploaded fasta file!<br><br>
	The sequence you have uploaded would probably block the MITOS webserver for a long time. <br>
	We would like to offer you to run the MITOS analysis on dedicated hardware that is independent of the webserver. <br>
	Please contact us at <a href="mailto:{email}">{email}</a>.<br><br>
	<a href="#" onClick="history.go(-1);return true;">Back to MITOS start page.</a>
</div>

<div id="invalidform" style="display:none">
	Invalid form data received. <br>
	Please contact us at <a href="mailto:{email}">{email}</a>.<br><br>
	<a href="#" onClick="history.go(-1);return true;">Back to MITOS start page.</a>
</div>

<div id="internal" style="display:none">
	Internal error!<br>
	Please report to the MITOS developers.<br>
</div>
